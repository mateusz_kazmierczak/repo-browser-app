package pl.mkazik.repobrowser.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

@Database(
        entities = [
                LocalRepo::class
        ],
        version = 1,
        exportSchema = false
)
@TypeConverters(DataConverters::class)
abstract class LocalDatabase : RoomDatabase() {
        abstract fun reposDao(): RepoDao
}
