package pl.mkazik.repobrowser.db

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import pl.mkazik.repobrowser.di.Qualifiers

@Module
class DbModule {

    @Provides
    fun provideDatabase(@Qualifiers.ForApp context: Context): LocalDatabase = Room
            .databaseBuilder(context, LocalDatabase::class.java, "repos.db")
            .fallbackToDestructiveMigration()
            .build()

}
