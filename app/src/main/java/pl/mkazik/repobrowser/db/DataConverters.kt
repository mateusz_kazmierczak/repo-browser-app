package pl.mkazik.repobrowser.db

import android.arch.persistence.room.TypeConverter
import android.net.Uri


object DataConverters {

    @TypeConverter
    @JvmStatic
    fun uriToString(uri: Uri): String =
            uri.toString()

    @TypeConverter
    @JvmStatic
    fun stringToUri(data: String?): Uri =
            Uri.parse(data)

    @TypeConverter
    @JvmStatic
    fun sourceEnumToString(source: Source): String =
            source.toString()

    @TypeConverter
    @JvmStatic
    fun stringToSourceEnum(data: String?): Source =
            Source.valueOf(data ?: "")
}
