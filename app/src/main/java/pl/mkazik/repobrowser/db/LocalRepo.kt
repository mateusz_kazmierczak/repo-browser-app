package pl.mkazik.repobrowser.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.net.Uri
import pl.mkazik.repobrowser.data.model.BucketRepo
import pl.mkazik.repobrowser.data.model.GithubRepo
import java.util.*

@Entity(tableName = "repos")
data class LocalRepo(
        @PrimaryKey val id: Long,
        val name: String,
        val owner: String,
        val ownerAvatarUri: Uri,
        val source: Source,
        val description: String? = null) {

    companion object {
        fun fromGithub(model: GithubRepo): LocalRepo =
                LocalRepo(UUID.randomUUID().hashCode().toLong(),
                        model.name,
                        model.owner.login,
                        model.owner.avatarUrl,
                        Source.GITHUB,
                        model.description
                )

        fun fromBitbucket(model: BucketRepo): LocalRepo =
                LocalRepo(UUID.randomUUID().hashCode().toLong(),
                        model.name,
                        model.owner.displayName,
                        model.owner.links.avatar.href,
                        Source.BIT_BUCKET,
                        model.description
                )

        fun fromMinimal(model: LocalRepoMinimal): LocalRepo =
                LocalRepo(model.id,
                        model.name,
                        model.owner,
                        model.ownerAvatarUri,
                        model.source
                )
    }

}

data class LocalRepoMinimal(
        val id: Long,
        val name: String,
        val owner: String,
        val ownerAvatarUri: Uri,
        val source: Source) {

    companion object {
        fun fromLocalRepo(model: LocalRepo): LocalRepoMinimal =
                LocalRepoMinimal(model.id,
                        model.name,
                        model.owner,
                        model.ownerAvatarUri,
                        model.source
                )
    }

}

enum class Source { GITHUB, BIT_BUCKET }