package pl.mkazik.repobrowser.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Single


@Dao
interface RepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg repos: LocalRepo)

    @Query("SELECT id, name, owner, ownerAvatarUri, source FROM repos")
    fun loadRepositories(): Flowable<List<LocalRepoMinimal>>

    @Query("SELECT * FROM repos WHERE id = :id")
    fun loadRepository(id: Long): Flowable<LocalRepo>

}
