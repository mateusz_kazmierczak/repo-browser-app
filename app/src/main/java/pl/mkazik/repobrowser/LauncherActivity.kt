package pl.mkazik.repobrowser

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.jakewharton.rxbinding2.view.RxView
import dagger.Provides
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_launcher.*
import pl.mkazik.repobrowser.di.BaseActivityModule
import pl.mkazik.repobrowser.view.details.DetailsActivity
import pl.mkazik.repobrowser.view.repolist.ReposAdapter
import pl.mkazik.repobrowser.view.repolist.RepositoriesListPresenter
import javax.inject.Inject

class LauncherActivity : DaggerAppCompatActivity() {

    @Inject lateinit var adapter: ReposAdapter
    @Inject lateinit var disposables: CompositeDisposable
    @Inject lateinit var presenter: RepositoriesListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        setSupportActionBar(toolbar)
        setupRecyclerView()
        setupSubscriptions()
    }

    private fun setupSubscriptions() {
        disposables.addAll(
                presenter.dataLoadSuccess
                        .subscribe(adapter),

                presenter.progress
                        .subscribe(RxView.visibility(progress)),

                presenter.selectedRepository
                        .subscribe { repoId ->
                            startActivity(DetailsActivity.newIntent(this, repoId))
                        }
        )
    }

    private fun setupRecyclerView() {
        repositories_list.layoutManager = LinearLayoutManager(this)
        repositories_list.setHasFixedSize(true)
        repositories_list.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    @dagger.Module(includes = [BaseActivityModule::class])
    class Module {

        @Provides
        fun provideAdapter(): ReposAdapter = ReposAdapter()

    }
}
