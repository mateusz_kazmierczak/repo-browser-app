package pl.mkazik.repobrowser.dao

import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import pl.mkazik.repobrowser.data.BitBucketService
import pl.mkazik.repobrowser.data.GithubService
import pl.mkazik.repobrowser.db.LocalDatabase
import pl.mkazik.repobrowser.db.LocalRepo
import pl.mkazik.repobrowser.db.LocalRepoMinimal
import javax.inject.Inject


class RepositoriesDao @Inject constructor(private val database: LocalDatabase,
                                          githubService: GithubService,
                                          bitBucketService: BitBucketService) {

    fun loadRepository(repoId: Long): Flowable<LocalRepo> = database
            .reposDao()
            .loadRepository(repoId)

    fun loadRepositories(): Flowable<List<LocalRepoMinimal>> = Flowable
            .concat(
                    database.reposDao()
                            .loadRepositories()
                            .firstElement()
                            .defaultIfEmpty(emptyList())
                            .toFlowable(),
                    allRepositories
                            .toFlowable()
                            .flatMapIterable { it }
                            .map { LocalRepoMinimal.fromLocalRepo(it) }
                            .toList()
                            .toFlowable()
            )
            .filter { it.isNotEmpty() }
            .firstOrError()
            .toFlowable()

    private val githubRepositories = githubService
            .getRepositories()
            .flattenAsFlowable { it }
            .map { LocalRepo.fromGithub(it) }
            .toList()

    private val bitBucketRepositories = bitBucketService
            .getRepositories()
            .map { repo -> repo.values }
            .flattenAsFlowable { it }
            .map { LocalRepo.fromBitbucket(it) }
            .toList()

    private val allRepositories = Single
            .zip<List<LocalRepo>, List<LocalRepo>, List<LocalRepo>>(
                    githubRepositories,
                    bitBucketRepositories,
                    BiFunction { github, bitBucket ->
                        github.plus(bitBucket)
                    }
            ).map { repos ->
                database.reposDao().insert(*repos.toTypedArray())
                repos
            }

}