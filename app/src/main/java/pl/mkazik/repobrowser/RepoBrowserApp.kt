package pl.mkazik.repobrowser

import android.content.Intent
import com.facebook.common.logging.FLog
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.security.ProviderInstaller
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import pl.mkazik.repobrowser.di.DaggerMainComponent
import timber.log.Timber

class RepoBrowserApp : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()
        setupFresco()
        setupProviderInstaller()
    }

    private fun setupProviderInstaller() {
        ProviderInstaller.installIfNeededAsync(this, object : ProviderInstaller.ProviderInstallListener {

            override fun onProviderInstallFailed(errorCode: Int, recoveryIntent: Intent) {
                val status = CommonStatusCodes.getStatusCodeString(errorCode)
                Timber.w("Error update security: $status, $recoveryIntent")
            }

            override fun onProviderInstalled() {
                Timber.i("Successfully updated security")
            }

        })
    }

    private fun setupTimber() {
        Timber.plant(Timber.DebugTree())
    }

    private fun setupFresco() {
        Fresco.initialize(this)
        FLog.setMinimumLoggingLevel(FLog.VERBOSE)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerMainComponent
                .builder()
                .create(this)
}