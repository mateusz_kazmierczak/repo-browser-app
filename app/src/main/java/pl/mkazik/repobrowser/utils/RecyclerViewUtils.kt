package pl.mkazik.repobrowser.utils

import android.support.v7.widget.RecyclerView
import android.view.View


abstract class BaseItem(val adapterId: Long = NO_ID) {

    companion object {
        const val NO_ID = -1L
    }

}

abstract class BaseViewHolder<T : BaseItem>(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(item: T)

    open fun onViewRecycled() {
    }

}
