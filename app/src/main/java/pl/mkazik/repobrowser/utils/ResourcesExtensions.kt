package pl.mkazik.repobrowser.utils

import android.content.res.Resources
import android.util.TypedValue

fun Resources.dpToPx(value: Float): Float =
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, displayMetrics)
