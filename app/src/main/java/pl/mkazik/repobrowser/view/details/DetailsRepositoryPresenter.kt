package pl.mkazik.repobrowser.view.details

import io.reactivex.Flowable
import pl.mkazik.repobrowser.dao.RepositoriesDao
import pl.mkazik.repobrowser.data.network.SchedulerProvider
import pl.mkazik.repobrowser.view.repolist.RepositoryItem
import javax.inject.Inject


class DetailsRepositoryPresenter @Inject constructor(repositoriesDao: RepositoriesDao,
                                                     schedulers: SchedulerProvider,
                                                     repoId: Long) {

    val dataLoadSuccess: Flowable<RepositoryItem> = repositoriesDao
            .loadRepository(repoId)
            .map { RepositoryItem.fromLocalRepo(it) }
            .observeOn(schedulers.ui())
            .subscribeOn(schedulers.io())

}
