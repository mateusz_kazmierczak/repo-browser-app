package pl.mkazik.repobrowser.view.repolist

import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor
import pl.mkazik.repobrowser.dao.RepositoriesDao
import pl.mkazik.repobrowser.data.network.SchedulerProvider
import pl.mkazik.repobrowser.utils.BaseItem
import javax.inject.Inject

class RepositoriesListPresenter @Inject constructor(repositoriesDao: RepositoriesDao,
                                                    schedulers: SchedulerProvider) {

    private val selectedPosition = PublishProcessor.create<Long>()

    val dataLoadSuccess: Flowable<List<BaseItem>> = repositoriesDao
            .loadRepositories()
            .flatMapIterable { it }
            .map { RepositoryItem.fromLocalRepoMinimal(it, selectedPosition) as BaseItem }
            .toList()
            .onErrorReturn { error -> listOf(ErrorView(error)) }
            .toFlowable()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

    val progress: Flowable<Boolean> = dataLoadSuccess.map { false }
            .startWith(true)
            .observeOn(schedulers.ui())

    val selectedRepository: Flowable<Long> = selectedPosition

}
