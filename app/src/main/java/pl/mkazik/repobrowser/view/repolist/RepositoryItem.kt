package pl.mkazik.repobrowser.view.repolist

import android.net.Uri
import io.reactivex.processors.PublishProcessor
import org.reactivestreams.Subscriber
import pl.mkazik.repobrowser.db.LocalRepo
import pl.mkazik.repobrowser.db.LocalRepoMinimal
import pl.mkazik.repobrowser.db.Source
import pl.mkazik.repobrowser.utils.BaseItem

data class RepositoryItem(
        val id: Long,
        val name: String,
        val username: String,
        val avatarUrl: Uri,
        val isBitBucket: Boolean,
        val clickHandler: Subscriber<Long>,
        val description: String?) : BaseItem() {

    companion object {
        fun fromLocalRepo(repo: LocalRepo): RepositoryItem =
                RepositoryItem(
                        repo.id,
                        repo.name,
                        repo.owner,
                        repo.ownerAvatarUri,
                        repo.source == Source.BIT_BUCKET,
                        PublishProcessor.create(),
                        repo.description
                )

        fun fromLocalRepoMinimal(repo: LocalRepoMinimal, selectedPosition: PublishProcessor<Long>): RepositoryItem =
                RepositoryItem(
                        repo.id,
                        repo.name,
                        repo.owner,
                        repo.ownerAvatarUri,
                        repo.source == Source.BIT_BUCKET,
                        selectedPosition,
                        null
                )
    }

}

data class ErrorView(
        val throwable: Throwable) : BaseItem()
