package pl.mkazik.repobrowser.view.repolist

import android.net.Uri
import android.support.graphics.drawable.VectorDrawableCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.item_error_view.view.*
import kotlinx.android.synthetic.main.item_repo_content.view.*
import pl.mkazik.repobrowser.R
import pl.mkazik.repobrowser.utils.BaseItem
import pl.mkazik.repobrowser.utils.BaseViewHolder


class ReposAdapter : ListAdapter<BaseItem, BaseViewHolder<*>>(AdapterChangesDetector()), Consumer<List<BaseItem>> {
    private val repositories = mutableListOf<BaseItem>()

    override fun accept(newData: List<BaseItem>) {
        repositories.addAll(newData)
        submitList(newData)
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return if (item is RepositoryItem) 0 else 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            0 -> ItemViewHolder(layoutInflater.inflate(R.layout.item_repo_content, parent, false))
            else -> ErrorViewHolder(layoutInflater.inflate(R.layout.item_error_view, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder.itemViewType) {
            0 -> (holder as ItemViewHolder).bind(getItem(position) as RepositoryItem)
            else -> (holder as ErrorViewHolder).bind(getItem(position) as ErrorView)
        }
    }

    override fun onViewRecycled(holder: BaseViewHolder<*>) {
        holder.onViewRecycled()
    }

    class ErrorViewHolder(view: View) : BaseViewHolder<ErrorView>(view) {
        override fun bind(item: ErrorView) {
            itemView.error_description.text = item.throwable.javaClass.simpleName
                    .plus(": ").plus(item.throwable.message)
        }
    }

    class ItemViewHolder(view: View) : BaseViewHolder<RepositoryItem>(view) {

        private val distinctionColor: Int
        private val defaultColor: Int
        private val compositeDisposable = CompositeDisposable()

        init {
            val theme = itemView.context.theme
            val res = itemView.resources
            val drawable = VectorDrawableCompat.create(res, R.drawable.user_avatar_placeholder, theme)
            itemView.user_avatar.hierarchy = GenericDraweeHierarchyBuilder(res)
                    .setPlaceholderImage(drawable)
                    .build()
            distinctionColor = ResourcesCompat.getColor(res, R.color.distinction, theme)
            defaultColor = ResourcesCompat.getColor(res, R.color.background_light, theme)
        }

        override fun bind(item: RepositoryItem) {
            compositeDisposable.addAll(
                    RxView.clicks(itemView)
                            .map { item.id }
                            .subscribe(item.clickHandler::onNext)
            )

            itemView.repo_name.text = item.name
            itemView.user_name.text = item.username
            itemView.user_avatar.controller = Fresco.newDraweeControllerBuilder()
                    .setOldController(itemView.user_avatar.controller)
                    .setImageRequest(prepareRequest(item.avatarUrl))
                    .build()
            itemView.card_view.setCardBackgroundColor(
                    if (item.isBitBucket) distinctionColor else defaultColor
            )
        }

        override fun onViewRecycled() {
            compositeDisposable.clear()
        }

        private fun prepareRequest(uri: Uri): ImageRequest =
                ImageRequestBuilder.newBuilderWithSource(uri)
                        .setResizeOptions(ResizeOptions.forSquareSize(72))
                        .setProgressiveRenderingEnabled(true)
                        .build()

    }

    private class AdapterChangesDetector : DiffUtil.ItemCallback<BaseItem>() {
        override fun areItemsTheSame(oldItem: BaseItem?, newItem: BaseItem?): Boolean =
                oldItem?.adapterId == newItem?.adapterId

        override fun areContentsTheSame(oldItem: BaseItem?, newItem: BaseItem?): Boolean =
                oldItem == newItem
    }
}
