package pl.mkazik.repobrowser.view.details

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.graphics.Palette
import android.view.MenuItem
import com.facebook.common.executors.CallerThreadExecutor
import com.facebook.common.references.CloseableReference
import com.facebook.datasource.DataSource
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber
import com.facebook.imagepipeline.image.CloseableImage
import com.facebook.imagepipeline.request.ImageRequest
import dagger.Provides
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_details.*
import pl.mkazik.repobrowser.R
import pl.mkazik.repobrowser.di.BaseActivityModule
import pl.mkazik.repobrowser.view.repolist.RepositoryItem
import timber.log.Timber
import javax.inject.Inject


class DetailsActivity : DaggerAppCompatActivity() {

    @Inject lateinit var presenter: DetailsRepositoryPresenter
    @Inject lateinit var disposables: CompositeDisposable

    companion object {
        private const val KEY_REPO_ID = "pl.mkazik.repobrowser.REPO_ID"

        fun newIntent(context: Context, repoId: Long): Intent =
                Intent(context, DetailsActivity::class.java).apply {
                    putExtra(KEY_REPO_ID, repoId)
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)
        setupToolbar()
        setupSubscriptions()
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupSubscriptions() {
        disposables.addAll(
                presenter.dataLoadSuccess
                        .subscribe { setupView(it) }
        )
    }

    private fun setupView(repo: RepositoryItem) {
        toolbar_layout.title = repo.name
        user_name.text = repo.username
        image_view.setImageURI(repo.avatarUrl, null)
        setupToolbarColor(repo.avatarUrl)
        scroll_container.setBackgroundColor(
                ResourcesCompat.getColor(
                        resources,
                        if (repo.isBitBucket) R.color.distinction else R.color.background_light,
                        theme
                )
        )
        repo_description.text = repo.description
    }

    private fun setupToolbarColor(uri: Uri) {
        val dataSource = Fresco.getImagePipeline().fetchDecodedImage(ImageRequest.fromUri(uri), null)
        try {
            dataSource.subscribe(object : BaseBitmapDataSubscriber() {
                override fun onFailureImpl(dataSource: DataSource<CloseableReference<CloseableImage>>?) {
                    Timber.e(dataSource?.failureCause, "Fail fetch cached bitmap")
                }

                override fun onNewResultImpl(bitmap: Bitmap?) {
                    Timber.d("Loaded $bitmap")
                    bitmap?.let {
                        val palette = Palette.from(bitmap).generate()
                        val swatch = palette.darkMutedSwatch
                        swatch?.titleTextColor?.let { textColor ->
                            toolbar_layout.setExpandedTitleColor(textColor)
                        }
                    }
                }
            }, CallerThreadExecutor.getInstance())
        } finally {
            dataSource.close()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    @dagger.Module(includes = [BaseActivityModule::class])
    class Module {

        @Provides
        fun provideRepoId(activity: DetailsActivity): Long =
                activity.intent.extras.getLong(KEY_REPO_ID)

    }

}