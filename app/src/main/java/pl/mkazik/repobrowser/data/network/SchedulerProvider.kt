package pl.mkazik.repobrowser.data.network

import io.reactivex.Scheduler

interface SchedulerProvider {
    fun ui(): Scheduler
    fun io(): Scheduler
    fun cpu(): Scheduler
}