package pl.mkazik.repobrowser.data.network

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Build
import com.google.gson.*
import com.moczul.ok2curl.CurlInterceptor
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import pl.mkazik.repobrowser.data.BitBucketService
import pl.mkazik.repobrowser.data.GithubService
import pl.mkazik.repobrowser.di.Qualifiers
import pl.mkazik.repobrowser.utils.Tls12SocketFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.security.KeyStore
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

@Module
class NetworkModule {

    @Provides
    fun provideSchedulers(): SchedulerProvider = object : SchedulerProvider {
        override fun io(): Scheduler = Schedulers.io()

        override fun ui(): Scheduler = AndroidSchedulers.mainThread()

        override fun cpu(): Scheduler = Schedulers.computation()
    }

    @Provides
    fun provideJsonUriDeserializer(): JsonDeserializer<Uri> = JsonDeserializer { json, _, _ ->
        if (json.isJsonPrimitive && json.asJsonPrimitive.isString) {
            return@JsonDeserializer Uri.parse(json.asString)
        }
        throw JsonParseException("Unknown uri string `$json`")
    }

    @Provides
    fun provideGson(uriJsonDeserializer: JsonDeserializer<Uri>): Gson =
            GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                    .registerTypeAdapter(Uri::class.java, uriJsonDeserializer)
                    .create()

    @Provides
    fun provideCache(@Qualifiers.ForApp context: Context): Cache =
            Cache(context.cacheDir, 10 * 1024 * 1024)

    @Provides
    fun provideOk2CurlInterceptor(): CurlInterceptor =
            CurlInterceptor { message ->
                Timber.tag(CurlInterceptor::class.java.simpleName).w(message)
            }

    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor =
            HttpLoggingInterceptor { message ->
                Timber.tag("HTTP").i(message)
            }.apply {
                level = HttpLoggingInterceptor.Level.BODY
            }

    @Provides
    fun provideOkHttp(cache: Cache,
                      ok2CurlInterceptor: CurlInterceptor,
                      loggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
            enableTls12OnPreLollipop(OkHttpClient.Builder()
                    .addNetworkInterceptor(loggingInterceptor)
                    .addInterceptor(ok2CurlInterceptor)
                    .cache(cache)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS))
                    .build()

    @SuppressLint("ObsoleteSdkInt")
    private fun enableTls12OnPreLollipop(okHttpClient: OkHttpClient.Builder): OkHttpClient.Builder {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
            try {
                val sslContext = SSLContext.getInstance("TLSv1.2")
                sslContext.init(null, null, null)
                okHttpClient.sslSocketFactory(
                        Tls12SocketFactory(sslContext.socketFactory),
                        defaultTrustManager()
                )

                val cs = ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build()
                okHttpClient.connectionSpecs(listOf(cs, ConnectionSpec.COMPATIBLE_TLS, ConnectionSpec.CLEARTEXT))
            } catch (e: Exception) {
                Timber.e(e, "OkHttpTLSCompat :: Failed while setting TLS 1.2")
            }
        }
        return okHttpClient
    }

    private fun defaultTrustManager(): X509TrustManager? {
        return try {
            val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(null as? KeyStore)
            val managers = trustManagerFactory.trustManagers
            val found = managers.find { it is X509TrustManager }
            return found as X509TrustManager?
        } catch (e: Exception) {
            null
        }
    }

    @Provides
    fun provideRetrofitBuilder(gson: Gson, okHttpClient: OkHttpClient): Retrofit.Builder = Retrofit
            .Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)

    @Provides
    fun provideGithubService(retrofitBuilder: Retrofit.Builder): GithubService = retrofitBuilder
            .baseUrl(HttpUrl.get("https://api.github.com/"))
            .build()
            .create(GithubService::class.java)

    @Provides
    fun provideBitBucketService(retrofitBuilder: Retrofit.Builder): BitBucketService = retrofitBuilder
            .baseUrl(HttpUrl.get("https://api.bitbucket.org/2.0/"))
            .build()
            .create(BitBucketService::class.java)

}