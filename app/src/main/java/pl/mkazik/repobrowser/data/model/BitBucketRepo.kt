package pl.mkazik.repobrowser.data.model

import android.net.Uri

data class BitBucketRepo(val values: List<BucketRepo>)

data class BucketRepo(
        val name: String,
        val description: String,
        val owner: BucketOwner)

data class BucketOwner(
        val username: String,
        val displayName: String,
        val links: Links)

data class Links(val avatar: Avatar)

data class Avatar(val href: Uri)
