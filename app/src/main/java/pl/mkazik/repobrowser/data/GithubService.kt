package pl.mkazik.repobrowser.data

import io.reactivex.Single
import pl.mkazik.repobrowser.data.model.GithubRepo
import retrofit2.http.GET


interface GithubService {

    @GET("repositories")
    fun getRepositories(): Single<List<GithubRepo>>

}