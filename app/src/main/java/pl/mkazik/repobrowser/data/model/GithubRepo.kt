package pl.mkazik.repobrowser.data.model

import android.net.Uri
import com.google.gson.annotations.SerializedName


data class GithubRepo(
        val id: Long,
        val name: String,
        val description: String,
        val owner: GithubOwner,
        @SerializedName("private") val isPrivate: Boolean
)

data class GithubOwner(
        val login: String,
        val avatarUrl: Uri
)
