package pl.mkazik.repobrowser.data

import io.reactivex.Single
import pl.mkazik.repobrowser.data.model.BitBucketRepo
import retrofit2.http.GET


interface BitBucketService {

    @GET("repositories?fields=values.name,values.owner,values.description")
    fun getRepositories(): Single<BitBucketRepo>

}
