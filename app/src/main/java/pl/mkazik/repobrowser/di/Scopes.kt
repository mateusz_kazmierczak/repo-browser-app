package pl.mkazik.repobrowser.di

import javax.inject.Scope

interface Scopes {
    @Scope
    @Retention
    annotation class Activity
}