package pl.mkazik.repobrowser.di

import android.app.Activity
import android.content.Context
import dagger.Binds
import dagger.Module


@Module
abstract class BaseActivityModule {
    @Binds
    @Qualifiers.ForActivity
    abstract fun activityContext(activity: Activity): Context
}