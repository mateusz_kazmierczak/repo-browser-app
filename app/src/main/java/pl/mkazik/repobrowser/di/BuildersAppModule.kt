package pl.mkazik.repobrowser.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.mkazik.repobrowser.LauncherActivity
import pl.mkazik.repobrowser.view.details.DetailsActivity

@Module
abstract class BuildersAppModule {
    @Scopes.Activity
    @ContributesAndroidInjector(modules = [LauncherActivity.Module::class])
    abstract fun mainActivityInjector(): LauncherActivity

    @Scopes.Activity
    @ContributesAndroidInjector(modules = [DetailsActivity.Module::class])
    abstract fun detailActivityInjector(): DetailsActivity
}
