package pl.mkazik.repobrowser.di

import dagger.Component
import dagger.android.AndroidInjector
import pl.mkazik.repobrowser.RepoBrowserApp
import pl.mkazik.repobrowser.db.DbModule
import pl.mkazik.repobrowser.data.network.NetworkModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    BuildersAppModule::class,
    NetworkModule::class,
    DbModule::class
])
interface MainComponent : AndroidInjector<RepoBrowserApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<RepoBrowserApp>()
}