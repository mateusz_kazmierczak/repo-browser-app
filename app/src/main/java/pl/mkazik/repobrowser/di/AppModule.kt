package pl.mkazik.repobrowser.di

import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.support.AndroidSupportInjectionModule
import io.reactivex.disposables.CompositeDisposable
import pl.mkazik.repobrowser.RepoBrowserApp

@Module(includes = [AndroidSupportInjectionModule::class])
abstract class AppModule {
    @Binds
    @Qualifiers.ForApp
    abstract fun applicationContext(app: RepoBrowserApp): Context

    @Module
    companion object {

        @Provides
        @JvmStatic
        fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    }

}