package pl.mkazik.repobrowser.di

import javax.inject.Qualifier

interface Qualifiers {

    @Qualifier
    @Retention
    annotation class ForApp

    @Qualifier
    @Retention
    annotation class ForActivity

}